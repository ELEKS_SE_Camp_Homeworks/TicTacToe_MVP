﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToe_MVP.Model;
using TicTacToe_MVP.View;

namespace TicTacToe_MVP.Presenter
{
    public class GamePresenter
    {
        private readonly GameView _gameView;
        private readonly WelcomeView _welcomeView;
        private readonly Player _playerOne;
        private readonly Player _playerTwo;
        private readonly IGameFieldModel _gameModel;

        public GamePresenter(GameView gameView, WelcomeView welcomeView, Player playerOne, Player playerTwo, IGameFieldModel gameFieldModel)
        {
            _gameView = gameView;
            _welcomeView = welcomeView;
            _playerOne = playerOne;
            _playerTwo = playerTwo;
            _gameModel = gameFieldModel;
            _welcomeView.Presenter = this;
            _gameView.Presenter = this;
        }

        public void ShowWelcomeView()
        {
            _welcomeView.ShowDialog();
        }

        public void ResetGame()
        {
            _playerOne.WinCount = 0;
            _playerTwo.WinCount = 0;
            NewGame();
        }

        public void SetPlayersData(string name)
        {
            if (_playerOne.Name.Equals(""))
            {
                _playerOne.Name = name;
                _gameView.PlayerOneLabel.Text = name;
                _gameView.PlayerOneScoreLabel.Text = _playerOne.WinCount.ToString();
                _welcomeView.Controls["PlayerLabel"].Text = @"Player2 name:";
            }
            else
            {
                _playerTwo.Name = name;
                _gameView.PlayerTwoLabel.Text = name;
                _gameView.PlayerTwoScoreLabel.Text = _playerTwo.WinCount.ToString();
                _welcomeView.Close();
                _gameView.CurrentPlayerLabel.Text = _playerOne.Name;
            }
        }

        public void NewGame()
        {
            _gameModel.ClearGameGrid();;
            var buttonTable = _gameView.GameTable;
            foreach (Button button in buttonTable.Controls)
            {
                button.Text = "";
            }
            _gameView.HintLabel.Text = "Move belongs:";
            _gameView.PlayerOneScoreLabel.Text = _playerOne.WinCount.ToString();
            _gameView.PlayerTwoScoreLabel.Text = _playerTwo.WinCount.ToString();
            _gameView.CurrentPlayerLabel.Text = _playerOne.Name;
            buttonTable.Enabled = true;
        }

        public void DeterminePlayerMove(int rowIndex,int colIndex)
        {
            if (_gameModel.GetSymbolFromCell(rowIndex, colIndex) != ' ')
                return;

            var buttonTable = _gameView.GameTable;
            char symbol = _gameModel.IsPlayerOneMove ? 'X' : 'O';
            _gameModel.SetCellSymbol(symbol,rowIndex,colIndex);

            Button currentClikedButton = buttonTable.GetControlFromPosition(colIndex, rowIndex) as Button;
            if (_gameModel.IsPlayerOneMove)
            {
                currentClikedButton.Text = _gameModel.GetSymbolFromCell(rowIndex, colIndex).ToString();
                _gameView.CurrentPlayerLabel.Text = _playerTwo.Name;
            }
            else
            {
                currentClikedButton.Text = _gameModel.GetSymbolFromCell(rowIndex, colIndex).ToString();
                _gameView.CurrentPlayerLabel.Text = _playerOne.Name;
            }
            _gameModel.IsPlayerOneMove = !_gameModel.IsPlayerOneMove;

            switch (ChekWinPlayer())
            {
                case 'X':
                    _gameView.HintLabel.Text = "Winner:";
                    _gameView.CurrentPlayerLabel.Text = _playerOne.Name;
                    ++_playerOne.WinCount;
                    _gameView.PlayerOneScoreLabel.Text = _playerOne.WinCount.ToString();
                    buttonTable.Enabled = false;
                    break;
                case 'O':
                    _gameView.HintLabel.Text = "Winner:";
                    _gameView.CurrentPlayerLabel.Text = _playerTwo.Name;
                    ++_playerTwo.WinCount;
                    _gameView.PlayerTwoScoreLabel.Text = _playerTwo.WinCount.ToString();
                    buttonTable.Enabled = false;
                    break;
                case 'd':
                    _gameView.HintLabel.Text = "No one won...It\'s drow";
                    _gameView.CurrentPlayerLabel.Text = "";
                    buttonTable.Enabled = false;
                    break;
            }
        }

        public char ChekWinPlayer()
        {
            for (int i = 0; i < 3; i++)
            {
                if (_gameModel.GetSymbolFromCell(i, 0) != ' ' 
                    && _gameModel.GetSymbolFromCell(i, 0) == _gameModel.GetSymbolFromCell(i, 1) 
                    && _gameModel.GetSymbolFromCell(i, 1) == _gameModel.GetSymbolFromCell(i, 2))
                {
                    return _gameModel.GetSymbolFromCell(i, 0);
                }
                if (_gameModel.GetSymbolFromCell(0, i) != ' ' 
                    && _gameModel.GetSymbolFromCell(0, i) == _gameModel.GetSymbolFromCell(1, i) 
                    && _gameModel.GetSymbolFromCell(1, i) == _gameModel.GetSymbolFromCell(2, i))
                {
                    
                    return _gameModel.GetSymbolFromCell(0, i);
                }   
            }

            if (_gameModel.GetSymbolFromCell(0, 0) != ' '
                && _gameModel.GetSymbolFromCell(0, 0) == _gameModel.GetSymbolFromCell(1, 1) 
                && _gameModel.GetSymbolFromCell(1, 1) == _gameModel.GetSymbolFromCell(2, 2))
            {
                return _gameModel.GetSymbolFromCell(1, 1);
            }
            if (_gameModel.GetSymbolFromCell(0, 2) != ' ' 
                && _gameModel.GetSymbolFromCell(0, 2) == _gameModel.GetSymbolFromCell(1, 1) 
                && _gameModel.GetSymbolFromCell(2, 0) == _gameModel.GetSymbolFromCell(1, 1))
            {
                return _gameModel.GetSymbolFromCell(1, 1);
            }

            for (int i = 0; i < 3; i++)
            {
                for (int j = 0; j < 3; j++)
                {
                    if (_gameModel.GetSymbolFromCell(i, j) == ' ')
                        return ' ';
                }
            }

            return 'd';
        }
    }
}
