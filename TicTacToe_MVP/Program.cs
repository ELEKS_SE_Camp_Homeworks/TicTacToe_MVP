﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToe_MVP.Model;
using TicTacToe_MVP.Presenter;
using TicTacToe_MVP.View;

namespace TicTacToe_MVP
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            var gameView = new GameView();
            var welcomeView = new WelcomeView();
            var presenter = new GamePresenter(gameView,welcomeView,new Player(),new Player(),new GameFieldModel());

            Application.Run(gameView);
        }
    }
}
