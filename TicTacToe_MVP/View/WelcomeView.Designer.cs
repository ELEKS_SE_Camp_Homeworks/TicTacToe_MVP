﻿namespace TicTacToe_MVP.View
{
    partial class WelcomeView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.PlayerNameBox = new System.Windows.Forms.TextBox();
            this.PlayerLabel = new System.Windows.Forms.Label();
            this.EnterNameBtn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(94, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 31);
            this.label1.TabIndex = 2;
            this.label1.Text = "Welcome! ";
            // 
            // PlayerNameBox
            // 
            this.PlayerNameBox.Font = new System.Drawing.Font("Times New Roman", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlayerNameBox.Location = new System.Drawing.Point(25, 80);
            this.PlayerNameBox.MaxLength = 15;
            this.PlayerNameBox.Name = "PlayerNameBox";
            this.PlayerNameBox.Size = new System.Drawing.Size(265, 29);
            this.PlayerNameBox.TabIndex = 3;
            // 
            // PlayerLabel
            // 
            this.PlayerLabel.AutoSize = true;
            this.PlayerLabel.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.PlayerLabel.Location = new System.Drawing.Point(84, 40);
            this.PlayerLabel.Name = "PlayerLabel";
            this.PlayerLabel.Size = new System.Drawing.Size(150, 27);
            this.PlayerLabel.TabIndex = 4;
            this.PlayerLabel.Text = "Player1 name:";
            // 
            // EnterNameBtn
            // 
            this.EnterNameBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.EnterNameBtn.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.EnterNameBtn.Location = new System.Drawing.Point(114, 115);
            this.EnterNameBtn.Name = "EnterNameBtn";
            this.EnterNameBtn.Size = new System.Drawing.Size(83, 26);
            this.EnterNameBtn.TabIndex = 5;
            this.EnterNameBtn.Text = "Enter";
            this.EnterNameBtn.UseVisualStyleBackColor = true;
            this.EnterNameBtn.Click += new System.EventHandler(this.EnterNameBtn_Click);
            // 
            // WelcomeView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(313, 153);
            this.Controls.Add(this.EnterNameBtn);
            this.Controls.Add(this.PlayerLabel);
            this.Controls.Add(this.PlayerNameBox);
            this.Controls.Add(this.label1);
            this.Name = "WelcomeView";
            this.Text = "WelcomeView";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.WelcomeView_FormClosing);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox PlayerNameBox;
        private System.Windows.Forms.Label PlayerLabel;
        private System.Windows.Forms.Button EnterNameBtn;
    }
}