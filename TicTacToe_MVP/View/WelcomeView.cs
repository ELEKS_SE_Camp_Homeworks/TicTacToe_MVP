﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToe_MVP.Presenter;

namespace TicTacToe_MVP.View
{
    public partial class WelcomeView : Form
    {
        public GamePresenter Presenter{ private get; set; }

        public WelcomeView()
        {
            InitializeComponent();
        }

        private void EnterNameBtn_Click(object sender, EventArgs e)
        {
            Presenter.SetPlayersData(PlayerNameBox.Text);
            PlayerNameBox.Text = "";
        }

        private void WelcomeView_FormClosing(object sender, FormClosingEventArgs e)
        {

        }

        private const int CP_NOCLOSE_BUTTON = 0x200;
        protected override CreateParams CreateParams
        {
            get
            {
                CreateParams myCp = base.CreateParams;
                myCp.ClassStyle = myCp.ClassStyle | CP_NOCLOSE_BUTTON;
                return myCp;
            }
        }
    }
}
