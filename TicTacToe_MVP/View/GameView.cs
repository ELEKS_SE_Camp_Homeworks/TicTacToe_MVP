﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using TicTacToe_MVP.Presenter;

namespace TicTacToe_MVP.View
{
    public partial class GameView : Form
    {
        public GamePresenter Presenter { private get; set; }
        public TableLayoutPanel GameTable {
            get { return gameTable; }
        }

        public Label CurrentPlayerLabel
        {
            get { return currentPlayerLabel; }
        }

        public Label HintLabel
        {
            get { return hintLabel; }
        }

        public Label PlayerOneLabel
        {
            get { return playerOneNameLabel; }
        }

        public Label PlayerTwoLabel
        {
            get { return playerTwoLabel; }
        }

        public Label PlayerOneScoreLabel
        {
            get { return scoreOne; }
        }

        public Label PlayerTwoScoreLabel
        {
            get { return scoreTwo; }
        }

        public GameView()
        {
            InitializeComponent();
            foreach (Button b in GameTable.Controls)
            {
                b.MouseClick += ButtonTableCell_MouseClick;
            }
        }

        private void ButtonTableCell_MouseClick(object sender, MouseEventArgs e)
        {
            int rowIndex = GameTable.GetRow(sender as Button);
            int colIndex = GameTable.GetColumn(sender as Button);
            Presenter.DeterminePlayerMove(rowIndex,colIndex);
        }

        private void GameView_Load(object sender, EventArgs e)
        {
            Presenter.ShowWelcomeView();
        }

        private void GameTable_Paint(object sender, PaintEventArgs e)
        {

        }

        private void restartGameBtn_Click(object sender, EventArgs e)
        {
            Presenter.NewGame();
        }

        private void resetScoreBtn_Click(object sender, EventArgs e)
        {
            Presenter.ResetGame();
        }
    }
}
