﻿using System.Windows.Forms;

namespace TicTacToe_MVP.View
{
    partial class GameView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.gameTable = new System.Windows.Forms.TableLayoutPanel();
            this.G9 = new System.Windows.Forms.Button();
            this.G8 = new System.Windows.Forms.Button();
            this.G7 = new System.Windows.Forms.Button();
            this.G6 = new System.Windows.Forms.Button();
            this.G5 = new System.Windows.Forms.Button();
            this.G4 = new System.Windows.Forms.Button();
            this.G3 = new System.Windows.Forms.Button();
            this.G2 = new System.Windows.Forms.Button();
            this.G1 = new System.Windows.Forms.Button();
            this.hintLabel = new System.Windows.Forms.Label();
            this.restartGameBtn = new System.Windows.Forms.Button();
            this.currentPlayerLabel = new System.Windows.Forms.Label();
            this.resetScoreBtn = new System.Windows.Forms.Button();
            this.playerOneNameLabel = new System.Windows.Forms.Label();
            this.playerTwoLabel = new System.Windows.Forms.Label();
            this.scoreOne = new System.Windows.Forms.Label();
            this.scoreTwo = new System.Windows.Forms.Label();
            this.gameTable.SuspendLayout();
            this.SuspendLayout();
            // 
            // gameTable
            // 
            this.gameTable.ColumnCount = 3;
            this.gameTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.gameTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.gameTable.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.gameTable.Controls.Add(this.G9, 2, 2);
            this.gameTable.Controls.Add(this.G8, 1, 2);
            this.gameTable.Controls.Add(this.G7, 0, 2);
            this.gameTable.Controls.Add(this.G6, 2, 1);
            this.gameTable.Controls.Add(this.G5, 1, 1);
            this.gameTable.Controls.Add(this.G4, 0, 1);
            this.gameTable.Controls.Add(this.G3, 2, 0);
            this.gameTable.Controls.Add(this.G2, 1, 0);
            this.gameTable.Controls.Add(this.G1, 0, 0);
            this.gameTable.Location = new System.Drawing.Point(12, 44);
            this.gameTable.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.gameTable.Name = "gameTable";
            this.gameTable.RowCount = 3;
            this.gameTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.gameTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.gameTable.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.gameTable.Size = new System.Drawing.Size(395, 398);
            this.gameTable.TabIndex = 0;
            this.gameTable.Paint += new System.Windows.Forms.PaintEventHandler(this.GameTable_Paint);
            // 
            // G9
            // 
            this.G9.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G9.Location = new System.Drawing.Point(265, 268);
            this.G9.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G9.Name = "G9";
            this.G9.Size = new System.Drawing.Size(125, 124);
            this.G9.TabIndex = 8;
            this.G9.UseVisualStyleBackColor = true;
            // 
            // G8
            // 
            this.G8.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G8.Location = new System.Drawing.Point(134, 268);
            this.G8.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G8.Name = "G8";
            this.G8.Size = new System.Drawing.Size(125, 124);
            this.G8.TabIndex = 7;
            this.G8.UseVisualStyleBackColor = true;
            // 
            // G7
            // 
            this.G7.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G7.Location = new System.Drawing.Point(3, 268);
            this.G7.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G7.Name = "G7";
            this.G7.Size = new System.Drawing.Size(125, 124);
            this.G7.TabIndex = 6;
            this.G7.UseVisualStyleBackColor = true;
            // 
            // G6
            // 
            this.G6.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G6.Location = new System.Drawing.Point(265, 136);
            this.G6.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G6.Name = "G6";
            this.G6.Size = new System.Drawing.Size(125, 124);
            this.G6.TabIndex = 5;
            this.G6.UseVisualStyleBackColor = true;
            // 
            // G5
            // 
            this.G5.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G5.Location = new System.Drawing.Point(134, 136);
            this.G5.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G5.Name = "G5";
            this.G5.Size = new System.Drawing.Size(125, 124);
            this.G5.TabIndex = 4;
            this.G5.UseVisualStyleBackColor = true;
            // 
            // G4
            // 
            this.G4.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G4.Location = new System.Drawing.Point(3, 136);
            this.G4.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G4.Name = "G4";
            this.G4.Size = new System.Drawing.Size(125, 124);
            this.G4.TabIndex = 3;
            this.G4.UseVisualStyleBackColor = true;
            // 
            // G3
            // 
            this.G3.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G3.Location = new System.Drawing.Point(265, 4);
            this.G3.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G3.Name = "G3";
            this.G3.Size = new System.Drawing.Size(125, 124);
            this.G3.TabIndex = 2;
            this.G3.UseVisualStyleBackColor = true;
            // 
            // G2
            // 
            this.G2.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G2.Location = new System.Drawing.Point(134, 4);
            this.G2.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G2.Name = "G2";
            this.G2.Size = new System.Drawing.Size(125, 124);
            this.G2.TabIndex = 1;
            this.G2.UseVisualStyleBackColor = true;
            // 
            // G1
            // 
            this.G1.Font = new System.Drawing.Font("Baskerville Old Face", 48F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.G1.Location = new System.Drawing.Point(3, 4);
            this.G1.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.G1.Name = "G1";
            this.G1.Size = new System.Drawing.Size(124, 124);
            this.G1.TabIndex = 0;
            this.G1.UseVisualStyleBackColor = true;
            // 
            // hintLabel
            // 
            this.hintLabel.AutoSize = true;
            this.hintLabel.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.hintLabel.Location = new System.Drawing.Point(9, 9);
            this.hintLabel.Name = "hintLabel";
            this.hintLabel.Size = new System.Drawing.Size(180, 31);
            this.hintLabel.TabIndex = 1;
            this.hintLabel.Text = "Move belongs: ";
            // 
            // restartGameBtn
            // 
            this.restartGameBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.restartGameBtn.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.restartGameBtn.Location = new System.Drawing.Point(146, 444);
            this.restartGameBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.restartGameBtn.Name = "restartGameBtn";
            this.restartGameBtn.Size = new System.Drawing.Size(125, 37);
            this.restartGameBtn.TabIndex = 2;
            this.restartGameBtn.Text = "Restart game";
            this.restartGameBtn.UseVisualStyleBackColor = true;
            this.restartGameBtn.Click += new System.EventHandler(this.restartGameBtn_Click);
            // 
            // currentPlayerLabel
            // 
            this.currentPlayerLabel.AutoSize = true;
            this.currentPlayerLabel.Font = new System.Drawing.Font("Times New Roman", 20.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.currentPlayerLabel.Location = new System.Drawing.Point(195, 9);
            this.currentPlayerLabel.Name = "currentPlayerLabel";
            this.currentPlayerLabel.Size = new System.Drawing.Size(0, 31);
            this.currentPlayerLabel.TabIndex = 3;
            // 
            // resetScoreBtn
            // 
            this.resetScoreBtn.FlatStyle = System.Windows.Forms.FlatStyle.System;
            this.resetScoreBtn.Font = new System.Drawing.Font("Times New Roman", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.resetScoreBtn.Location = new System.Drawing.Point(146, 489);
            this.resetScoreBtn.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.resetScoreBtn.Name = "resetScoreBtn";
            this.resetScoreBtn.Size = new System.Drawing.Size(125, 36);
            this.resetScoreBtn.TabIndex = 4;
            this.resetScoreBtn.Text = "Reset score";
            this.resetScoreBtn.UseVisualStyleBackColor = true;
            this.resetScoreBtn.Click += new System.EventHandler(this.resetScoreBtn_Click);
            // 
            // playerOneNameLabel
            // 
            this.playerOneNameLabel.AutoSize = true;
            this.playerOneNameLabel.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.playerOneNameLabel.Location = new System.Drawing.Point(15, 446);
            this.playerOneNameLabel.MinimumSize = new System.Drawing.Size(125, 0);
            this.playerOneNameLabel.Name = "playerOneNameLabel";
            this.playerOneNameLabel.Size = new System.Drawing.Size(125, 27);
            this.playerOneNameLabel.TabIndex = 5;
            this.playerOneNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // playerTwoLabel
            // 
            this.playerTwoLabel.AutoSize = true;
            this.playerTwoLabel.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.playerTwoLabel.Location = new System.Drawing.Point(277, 446);
            this.playerTwoLabel.MinimumSize = new System.Drawing.Size(125, 0);
            this.playerTwoLabel.Name = "playerTwoLabel";
            this.playerTwoLabel.Size = new System.Drawing.Size(125, 27);
            this.playerTwoLabel.TabIndex = 6;
            this.playerTwoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scoreOne
            // 
            this.scoreOne.AutoSize = true;
            this.scoreOne.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scoreOne.Location = new System.Drawing.Point(15, 489);
            this.scoreOne.MinimumSize = new System.Drawing.Size(125, 0);
            this.scoreOne.Name = "scoreOne";
            this.scoreOne.Size = new System.Drawing.Size(125, 27);
            this.scoreOne.TabIndex = 7;
            this.scoreOne.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // scoreTwo
            // 
            this.scoreTwo.AutoSize = true;
            this.scoreTwo.Font = new System.Drawing.Font("Times New Roman", 18F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.scoreTwo.Location = new System.Drawing.Point(277, 489);
            this.scoreTwo.MinimumSize = new System.Drawing.Size(125, 0);
            this.scoreTwo.Name = "scoreTwo";
            this.scoreTwo.Size = new System.Drawing.Size(125, 27);
            this.scoreTwo.TabIndex = 8;
            this.scoreTwo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GameView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 529);
            this.Controls.Add(this.scoreTwo);
            this.Controls.Add(this.scoreOne);
            this.Controls.Add(this.playerTwoLabel);
            this.Controls.Add(this.playerOneNameLabel);
            this.Controls.Add(this.resetScoreBtn);
            this.Controls.Add(this.currentPlayerLabel);
            this.Controls.Add(this.restartGameBtn);
            this.Controls.Add(this.hintLabel);
            this.Controls.Add(this.gameTable);
            this.Margin = new System.Windows.Forms.Padding(3, 4, 3, 4);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(437, 552);
            this.Name = "GameView";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "TicTacToe Game";
            this.Load += new System.EventHandler(this.GameView_Load);
            this.gameTable.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private TableLayoutPanel gameTable;
        private Button G9;
        private Button G8;
        private Button G7;
        private Button G6;
        private Button G5;
        private Button G4;
        private Button G3;
        private Button G2;
        private Button G1;
        private Label hintLabel;
        private Button restartGameBtn;
        private Label currentPlayerLabel;
        private Button resetScoreBtn;
        private Label playerOneNameLabel;
        private Label playerTwoLabel;
        private Label scoreOne;
        private Label scoreTwo;
    }
}