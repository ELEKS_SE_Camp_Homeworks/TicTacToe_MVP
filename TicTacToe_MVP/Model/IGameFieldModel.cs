﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_MVP.Model
{
    public interface IGameFieldModel
    {
        void ClearGameGrid();

        void SetCellSymbol(char cellSymbol, int rowIndex, int colIndex);

        char GetSymbolFromCell(int rowIndex, int colIndex);

        bool IsPlayerOneMove { get; set; }
    }
}
