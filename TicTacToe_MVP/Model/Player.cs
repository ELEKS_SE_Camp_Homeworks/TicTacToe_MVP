﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_MVP.Model
{
    public class Player
    {
        public string Name { get; set; }
        public int WinCount { get; set; }

        public Player()
        {
            Name = "";
            WinCount = 0;
        }

        public Player(string name)
        {
            Name = name;
            WinCount = 0;

        }
    }
}
