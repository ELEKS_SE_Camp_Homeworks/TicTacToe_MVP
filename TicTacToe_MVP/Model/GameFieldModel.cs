﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TicTacToe_MVP.Model
{
    public class GameFieldModel: IGameFieldModel
    {
        private char[,] _gameGrid;
        public bool IsPlayerOneMove { get; set; }

        public GameFieldModel()
        {
            _gameGrid = new char[3, 3];
            ClearGameGrid();
            IsPlayerOneMove = true;
        }

        public void ClearGameGrid()
        {
            for (int i = 0; i < _gameGrid.GetLength(0); i++)
            {
                for (int j = 0; j < _gameGrid.GetLength(0); j++)
                {
                    _gameGrid[i, j] = ' ';
                }
            }
            IsPlayerOneMove = true;
        }
    
        public void SetCellSymbol(char cellSymbol, int rowIndex, int colIndex)
        {
            _gameGrid[rowIndex, colIndex] = cellSymbol;
        }
 
        public char GetSymbolFromCell(int rowIndex, int colIndex)
        {
            return _gameGrid[rowIndex, colIndex];
        }
    }
}
